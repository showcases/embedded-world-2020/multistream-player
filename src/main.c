#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <wayland-client-core.h>
#include <wayland-client-protocol.h>
#include "xdg-shell-client-protocol.h"

#include <pixman.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <wayland-egl.h>

#include <gst/gst.h>
#include <gst/video/videooverlay.h>

#include "wayland.h"
#include "weston-os-compatibility.h"
#include "weston-image-loader.h"
#include "gresource-multistream-player.h"

#define MSS_SERVER "192.168.42.236"
#define STREAM_WIDTH 1280
#define STREAM_HEIGHT 720
#define STREAM_CAPTION 170
#define OUTPUT_WIDTH 3840
#define OUTPUT_HEIGHT 2160
#define VIDEO_SINK "waylandsink"
#define HW_DECODER "v4l2slh264dec"

typedef struct {
  const gchar *pipeline_str;
  gint initial_x;
  gint initial_y;
  gint cur_x;
  gint cur_y;
  GstElement *pipeline;
  struct wl_surface *wl_surface;
  struct wl_subsurface *wl_subsurface;
  const char *resource_name;
  gboolean needs_mss_server;
} MssStream;

static MssStream streams_data[] = {
  {
    .resource_name = "/srt.png",
    .initial_x = 139,
    .initial_y = 139,
    .pipeline_str = "srtsrc uri=srt://%s:7002 latency=50 do-timestamp=1 "
      "! tsdemux latency=50 ! h264parse ! " HW_DECODER,
    .needs_mss_server = TRUE,
  },
  {
    .resource_name = "/rist.png",
    .initial_x = 139,
    .initial_y = 1300,
    .pipeline_str =
      "ristsrc address=224.0.0.1 port=5004 receiver-buffer=50 "
              "stats-update-interval=2000 rist_rtp_udpsrc0::buffer-size=2000000 "
        "! rtpmp2tdepay ! tsdemux latency=50 ! h264parse ! " HW_DECODER,
    .needs_mss_server = FALSE,
  },
  {
    .resource_name = "/hls.png",
    .initial_x = 2420,
    .initial_y = 1300,
    .pipeline_str = "souphttpsrc location=http://%s:8080/hls/playlist.m3u8 "
    "! hlsdemux ! tsdemux ! h264parse ! " HW_DECODER,
    .needs_mss_server = TRUE,
  },
};

typedef struct {
  int32_t touch_id;
  MssStream *stream;
  uint32_t down_time;
  uint32_t up_time;
  int first_x;
  int first_y;
  int last_x;
  int last_y;
} MssTouch;

typedef struct {
  GApplication *app;
  GResource *resources;
  gboolean should_quit;
  gboolean videotestsrc;
  const gchar *mss_server;

  struct {
    struct wl_display *wl_display;
    struct wl_registry *wl_registry;
    struct wl_compositor *wl_compositor;
    struct wl_subcompositor *wl_subcompositor;
    struct wl_output *wl_output;
    guint output_scale; /* GSt doesn't actually support this ... */
    struct xdg_wm_base *xdg_wm_base;
    struct wl_shm *wl_shm;
    struct wl_seat *wl_seat;
    struct wl_touch *wl_touch;
    gboolean argb8888_supported;
    struct wl_buffer *blank_buffer;
    EGLDisplay egl_display;
    gint roundtrips_required;
    MssTouch touches[16];
  } dpy;

  struct {
    struct wl_surface *wl_surface;
    struct xdg_surface *xdg_surface;
    struct xdg_toplevel *xdg_toplevel;
    struct wl_egl_window *wl_egl_window;
    EGLSurface egl_surface;
    EGLContext egl_context;
    EGLConfig egl_config;
    GLuint gl_tex[G_N_ELEMENTS (streams_data) + 1];
    GLuint gl_pos;
    GLuint gl_texcoord;
    GLuint gl_tex_uni;
    gboolean commit_required;
    gboolean repaint_required;
    struct wl_callback *frame;
    uint32_t ack_configure;
  } win;
} MssPlayer;

static MssPlayer player = { .mss_server = MSS_SERVER, };

static gboolean setup_stream (MssStream *stream);

/* taken from weston/shared/platform.h */
static gboolean
check_egl_extension(const char *extensions, const char *extension)
{
  size_t extlen = strlen(extension);
  const char *end = extensions + strlen(extensions);

  while (extensions < end) {
    size_t n = 0;

    /* Skip whitespaces, if any */
    if (*extensions == ' ') {
      extensions++;
      continue;
    }

    n = strcspn(extensions, " ");

    /* Compare strings */
    if (n == extlen && strncmp(extension, extensions, n) == 0)
      return TRUE; /* Found */

    extensions += n;
  }

  /* Not found */
  return FALSE;
}

static gboolean
validate_system (void)
{
  gboolean ret = TRUE;

  if (!player.dpy.wl_output) {
    g_print ("This demo was designed for a 4K display.\n");
    ret = FALSE;
  }

  if (!player.dpy.argb8888_supported) {
    g_print ("The compositor must support wl_shm ARGB8888.\n");
    ret = FALSE;
  }

  return ret;
}

static gboolean
load_png_to_tex (const char *name, GLuint tex_idx,
                 int expected_width, int expected_height)
{
  GBytes *png;
  GError *error = NULL;
  gconstpointer png_data;
  gsize png_size;
  void *buffer_data;
  int width, height, stride;
  int fd;

  png = g_resource_lookup_data (player.resources, name,
      G_RESOURCE_LOOKUP_FLAGS_NONE, &error);
  if (!png) {
    g_print ("Couldn't load background: %s\n", error->message);
    g_error_free (error);
    return FALSE;
  }
  png_data = g_bytes_get_data (png, &png_size);
  fd = load_png (png_data, png_size, &width, &height, &stride);
  if (width != expected_width || height != expected_height) {
    g_print ("Resource size incorrect: %d x %d, expected %d x %d\n",
        width, height, expected_width, expected_height);
    return FALSE;
  }

  /* create a wl_buffer for our background */
  buffer_data = mmap (NULL, stride * height, PROT_READ | PROT_WRITE,
      MAP_SHARED, fd, 0);
  if (buffer_data == MAP_FAILED) {
    g_print ("Couldn't mmap decoded PNG buffer: %m\n");
    return FALSE;
  }

  glBindTexture (GL_TEXTURE_2D, player.win.gl_tex[tex_idx]);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glPixelStorei (GL_UNPACK_ROW_LENGTH_EXT, stride / 4);
  glPixelStorei (GL_UNPACK_SKIP_ROWS_EXT, 0);
  glPixelStorei (GL_UNPACK_SKIP_PIXELS_EXT, 0);
  glTexImage2D (GL_TEXTURE_2D, 0, GL_BGRA_EXT, width, height, 0, GL_BGRA_EXT,
      GL_UNSIGNED_BYTE, buffer_data);
  munmap (buffer_data, height * stride);

  return TRUE;
}

static gboolean
play_stream (MssStream *stream)
{
  gst_element_set_state (stream->pipeline, GST_STATE_PLAYING);
  return G_SOURCE_REMOVE;
}

static void
error_cb (GstBus * bus, GstMessage * msg, gpointer user_data)
{
  MssStream *stream = user_data;
  g_autofree gchar *debug = NULL;
  g_autoptr (GError) err = NULL;

  gst_message_parse_error (msg, &err, &debug);

  g_print ("Error: %s\n", err->message);
  if (debug)
    g_print ("Debug details: %s\n", debug);

  gst_element_set_state (stream->pipeline, GST_STATE_NULL);
  g_clear_object (&stream->pipeline);

  /* Retry in 3s...*/
  g_timeout_add_seconds (3, (GSourceFunc) setup_stream, stream);
}

static GstBusSyncReply
bus_sync_handler (GstBus * bus, GstMessage * message, gpointer user_data)
{
  MssStream *stream = user_data;

  if (gst_is_wayland_display_handle_need_context_message (message)) {
    GstContext *context;

    context = gst_wayland_display_handle_context_new (player.dpy.wl_display);
    gst_element_set_context (GST_ELEMENT (GST_MESSAGE_SRC (message)), context);

    goto drop;
  } else if (gst_is_video_overlay_prepare_window_handle_message (message)) {
    GstVideoOverlay *overlay;

    overlay = GST_VIDEO_OVERLAY (GST_MESSAGE_SRC (message));
    gst_video_overlay_set_window_handle (overlay, (guintptr) stream->wl_surface);
    gst_video_overlay_set_render_rectangle (overlay, 0, 0,
        STREAM_WIDTH / player.dpy.output_scale,
        STREAM_HEIGHT / player.dpy.output_scale);

    goto drop;
  }

  return GST_BUS_PASS;

drop:
  gst_message_unref (message);
  return GST_BUS_DROP;
}

static gboolean
setup_stream (MssStream *stream)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GstBus) bus = NULL;
  g_autoptr (GString) pipeline_str = g_string_new (NULL);

  /* create a wl_surface and subsurface for our output, so we can position
   * it below the background */
  if (!stream->wl_surface) {
    struct wl_region *opaque =
      wl_compositor_create_region (player.dpy.wl_compositor);
    stream->cur_x = stream->initial_x;
    stream->cur_y = stream->initial_y;
    stream->wl_surface =
       wl_compositor_create_surface (player.dpy.wl_compositor);
    wl_region_add (opaque, 0, 0, 1, 1);
    wl_surface_set_opaque_region (stream->wl_surface, opaque);
    wl_region_destroy (opaque);
    wl_surface_set_buffer_scale (stream->wl_surface, player.dpy.output_scale);
    wl_surface_attach (stream->wl_surface, player.dpy.blank_buffer, 0, 0);
    wl_surface_damage_buffer (stream->wl_surface, 0, 0, 1, 1);
    stream->wl_subsurface =
       wl_subcompositor_get_subsurface (player.dpy.wl_subcompositor,
           stream->wl_surface, player.win.wl_surface);
    wl_subsurface_set_position (stream->wl_subsurface,
        stream->cur_x / player.dpy.output_scale,
        stream->cur_y / player.dpy.output_scale);
    wl_subsurface_place_below (stream->wl_subsurface, player.win.wl_surface);
    wl_subsurface_set_desync (stream->wl_subsurface);
    wl_surface_commit (stream->wl_surface);
  }

  if (player.videotestsrc)
    g_string_append (pipeline_str, "videotestsrc");
  else if (stream->needs_mss_server)
    g_string_append_printf (pipeline_str, stream->pipeline_str, player.mss_server);
  else
    g_string_append (pipeline_str, stream->pipeline_str);

  g_string_append (pipeline_str, " ! " VIDEO_SINK);

  stream->pipeline = gst_parse_launch (pipeline_str->str, &error);
  if (!stream->pipeline) {
    player.should_quit = TRUE;
    g_print ("Failed to create stream pipeline: %s", error->message);
    return G_SOURCE_REMOVE;
  }

  bus = gst_pipeline_get_bus (GST_PIPELINE (stream->pipeline));
  gst_bus_add_signal_watch (bus);
  g_signal_connect (bus, "message::error", G_CALLBACK (error_cb), stream);
  gst_bus_set_sync_handler (bus, bus_sync_handler, stream, NULL);

  play_stream (stream);

  return G_SOURCE_REMOVE;
}

static void
setup_streams ()
{
  gint i;

  for (i = 0; i < G_N_ELEMENTS (streams_data); i++) {
    MssStream *stream = &streams_data[i];

    setup_stream (stream);
  }
}

static void
stop_streams ()
{
  gint i;
  for (i = 0; i < G_N_ELEMENTS (streams_data); i++) {
    MssStream *stream = &streams_data[i];
    if (stream->pipeline) {
      gst_element_set_state (stream->pipeline, GST_STATE_NULL);
      g_clear_object (&stream->pipeline);
    }
  }
}

static const char *vert_shader_text =
  "precision highp float;\n"
  "attribute vec2 in_pos;\n"
  "attribute vec2 in_texcoord;\n"
  "varying vec2 v_texcoord;\n"
  "void main() {\n"
  "  gl_Position = vec4(in_pos, 0.0, 1.0);\n"
  "  v_texcoord = in_texcoord;\n"
  "}\n";

static const char *frag_shader_text =
  "precision highp float;\n"
  "varying vec2 v_texcoord;\n"
  "uniform sampler2D tex;\n"
  "void main() {\n"
  "  gl_FragColor = texture2D(tex, v_texcoord);\n"
  "}\n";

static void
draw_gl (int tex_idx, int x, int y, int w, int h)
{
  const GLfloat left = ((x * 2) / (float) OUTPUT_WIDTH) - 1.0;
  const GLfloat right = (((x + w) * 2) / (float) OUTPUT_WIDTH) - 1.0;
  const GLfloat bottom = ((((y + h) * 2) / (float) OUTPUT_HEIGHT) - 1.0) * -1.0;
  const GLfloat top = (((y * 2) / (float) OUTPUT_HEIGHT) - 1.0) * -1.0;
  const GLfloat verts[] = {
    left,  bottom,
    left,  top,
    right, top,
    right, bottom,
  };
    
  glVertexAttribPointer (player.win.gl_pos, 2, GL_FLOAT, GL_FALSE, 0, verts);
  glBindTexture (GL_TEXTURE_2D, player.win.gl_tex[tex_idx]);
  glDrawArrays (GL_TRIANGLE_FAN, 0, 4);
}

static void
frame_done (void *data, struct wl_callback *frame, uint32_t time)
{
  if (frame != player.win.frame) {
    g_print ("Lost track of frame callbacks. Oh dear.\n");
    player.should_quit = TRUE;
    return;
  }

  player.win.frame = NULL;
}

static const struct wl_callback_listener frame_listener = {
  frame_done,
};

static void
repaint (void)
{
  static const GLfloat tex_coords[] = {
     0.0,  1.0,
     0.0,  0.0,
     1.0,  0.0,
     1.0,  1.0,
  };
  pixman_region32_t pm_opaque;
  struct wl_region *wl_opaque;
  pixman_box32_t *rects;
  int n_rects;

  if (player.win.frame) {
    g_print ("Lost track of frame callbacks. Oh dear.\n");
    player.should_quit = TRUE;
    return;
  }

  glVertexAttribPointer (player.win.gl_texcoord, 2, GL_FLOAT, GL_FALSE, 0,
      tex_coords);
  glEnableVertexAttribArray (player.win.gl_pos);
  glEnableVertexAttribArray (player.win.gl_texcoord);

  /* First draw the background layer */
  glDisable (GL_BLEND);
  glDisable (GL_SCISSOR_TEST);
  draw_gl (0, 0, 0, OUTPUT_WIDTH, OUTPUT_HEIGHT);

  /* Now draw the transparent overlays for the stream-type logos */
  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  for (int i = 0; i < G_N_ELEMENTS (streams_data); i++) {
    MssStream *stream = &streams_data[i];
    if (!stream->pipeline)
      continue;
    draw_gl (i + 1, stream->cur_x, stream->cur_y + STREAM_HEIGHT,
        STREAM_WIDTH, STREAM_CAPTION);
  }

  /* Now punch alpha holes through the already-drawn background for the streams */
  glDisable (GL_BLEND);
  glEnable (GL_SCISSOR_TEST);
  for (int i = 0; i < G_N_ELEMENTS (streams_data); i++) {
    MssStream *stream = &streams_data[i];
    if (!stream->pipeline)
      continue;
    glScissor (stream->cur_x, OUTPUT_HEIGHT - (stream->cur_y + STREAM_HEIGHT),
        STREAM_WIDTH, STREAM_HEIGHT);
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glClear (GL_COLOR_BUFFER_BIT);
  }

  /* create the opaque region, cutting out subsurfaces */
  pixman_region32_init_rect (&pm_opaque, 0, 0,
      OUTPUT_WIDTH / player.dpy.output_scale,
      OUTPUT_HEIGHT / player.dpy.output_scale);
  for (int i = 0; i < G_N_ELEMENTS (streams_data); i++) {
    MssStream *stream = &streams_data[i];
    pixman_region32_t subtract;

    if (!stream->pipeline)
      continue;

    pixman_region32_init_rect (&subtract,
        stream->cur_x / player.dpy.output_scale,
        stream->cur_y / player.dpy.output_scale,
        STREAM_WIDTH / player.dpy.output_scale,
        STREAM_HEIGHT / player.dpy.output_scale);
    pixman_region32_subtract (&pm_opaque, &pm_opaque, &subtract);
    pixman_region32_fini (&subtract);

    wl_subsurface_set_position (stream->wl_subsurface,
        stream->cur_x / player.dpy.output_scale,
        stream->cur_y / player.dpy.output_scale);
  }

  wl_opaque = wl_compositor_create_region (player.dpy.wl_compositor);
  rects = pixman_region32_rectangles (&pm_opaque, &n_rects);
  for (int i = 0; i < n_rects; i++) {
    wl_region_add (wl_opaque, rects[i].x1, rects[i].y1,
                   rects[i].x2 - rects[i].x1,
                   rects[i].y2 - rects[i].y1);
  }
  wl_surface_set_opaque_region (player.win.wl_surface, wl_opaque);
  wl_region_destroy (wl_opaque);
  pixman_region32_fini (&pm_opaque);

  if (player.win.ack_configure != 0) {
    xdg_surface_ack_configure (player.win.xdg_surface, player.win.ack_configure);
    player.win.ack_configure = 0;
  }

  player.win.repaint_required = FALSE;
  player.win.frame = wl_surface_frame (player.win.wl_surface);
  wl_callback_add_listener (player.win.frame, &frame_listener, NULL);
  eglSwapBuffers (player.dpy.egl_display, player.win.egl_surface);
}

/* always sent after xdg_toplevel_configure */
static void
xdg_surface_configure (void *data, struct xdg_surface *xdg_surface,
                       uint32_t serial)
{
  player.win.ack_configure = serial;
  player.win.repaint_required = TRUE;
}

static const struct xdg_surface_listener xdg_surface_listener = {
  xdg_surface_configure
};

/* the compositor tells us how to set up our window */
static void
xdg_toplevel_configure (void *data, struct xdg_toplevel *toplevel,
                        int width, int height, struct wl_array *states)
{
  uint32_t *s;
  gboolean fullscreen = FALSE;
  gint desired_width = OUTPUT_WIDTH / player.dpy.output_scale;
  gint desired_height = OUTPUT_HEIGHT / player.dpy.output_scale;

  wl_array_for_each(s, states) {
    if (*s == XDG_TOPLEVEL_STATE_FULLSCREEN)
      fullscreen = TRUE;
  }

  if (!fullscreen) {
    g_print ("Compositor will not let us go fullscreen.\n");
    player.should_quit = TRUE;
  }

  if (width != desired_width || height != desired_height) {
    g_print ("Compositor wants us to be %d x %d, should be %d x %d\n",
        width, height, desired_width, desired_height);
    player.should_quit = TRUE;
  }
}

static void
xdg_toplevel_close (void *data, struct xdg_toplevel *toplevel)
{
  g_print ("Closing gracefully\n");
  player.should_quit = TRUE;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
  xdg_toplevel_configure,
  xdg_toplevel_close
};

static void
xdg_wm_base_ping (void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial)
{
  xdg_wm_base_pong (xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener xdg_wm_base_listener = {
  xdg_wm_base_ping
};

static void
setup_surfaces ()
{
  PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC create_egl_surface;
  struct wl_shm_pool *wl_shm_pool;
  long pagesize;
  int fd;
  void *buffer_data;
  gint i;
  GLuint vert, frag, prog;
  GLint status;

  /* toplevel window */
  player.win.wl_surface =
      wl_compositor_create_surface (player.dpy.wl_compositor);
  wl_surface_set_buffer_scale (player.win.wl_surface,
      player.dpy.output_scale);
  player.win.xdg_surface = xdg_wm_base_get_xdg_surface (player.dpy.xdg_wm_base,
      player.win.wl_surface);
  xdg_surface_add_listener (player.win.xdg_surface, &xdg_surface_listener,
    NULL);
  player.win.xdg_toplevel = xdg_surface_get_toplevel (player.win.xdg_surface);
  xdg_toplevel_add_listener (player.win.xdg_toplevel, &xdg_toplevel_listener,
    NULL);
  xdg_toplevel_set_title (player.win.xdg_toplevel, "Multi-Stream Player");
  xdg_toplevel_set_app_id (player.win.xdg_toplevel,
      "com.collabora.multistream-player");
  xdg_toplevel_set_fullscreen (player.win.xdg_toplevel, player.dpy.wl_output);

  /* background EGLSurface */
  player.win.wl_egl_window = wl_egl_window_create (player.win.wl_surface,
    OUTPUT_WIDTH, OUTPUT_HEIGHT);
  if (!player.win.wl_egl_window) {
    g_print ("Couldn't create wl_egl_window.\n");
    player.should_quit = TRUE;
    return;
  }
  create_egl_surface =
      (PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC) eglGetProcAddress ("eglCreatePlatformWindowSurfaceEXT");
  player.win.egl_surface = create_egl_surface (player.dpy.egl_display,
      player.win.egl_config, player.win.wl_egl_window, NULL);
  if (player.win.egl_surface == EGL_NO_SURFACE) {
    g_print ("Couldn't create EGL surface\n");
    player.should_quit = TRUE;
    return;
  }

  /* GL texture & program resources */
  if (eglMakeCurrent (player.dpy.egl_display, player.win.egl_surface,
          player.win.egl_surface, player.win.egl_context) == EGL_FALSE) {
    g_print ("Couldn't make EGL context current\n");
    player.should_quit = TRUE;
    return;
  }

  glActiveTexture (GL_TEXTURE0);
  glGenTextures (G_N_ELEMENTS (player.win.gl_tex), player.win.gl_tex);

  vert = glCreateShader (GL_VERTEX_SHADER);
  glShaderSource (vert, 1, &vert_shader_text, NULL);
  glCompileShader (vert);
  glGetShaderiv (vert, GL_COMPILE_STATUS, &status);
  if (status != GL_TRUE) {
    g_print ("Couldn't compile vertex shader.\n");
    player.should_quit = TRUE;
    return;
  }
  frag = glCreateShader (GL_FRAGMENT_SHADER);
  glShaderSource (frag, 1, &frag_shader_text, NULL);
  glCompileShader (frag);
  glGetShaderiv (frag, GL_COMPILE_STATUS, &status);
  if (status != GL_TRUE) {
    g_print ("Couldn't compile vertex shader.\n");
    player.should_quit = TRUE;
    return;
  }
  prog = glCreateProgram ();
  glAttachShader (prog, vert);
  glAttachShader (prog, frag);
  glLinkProgram (prog);
  glGetProgramiv (prog, GL_LINK_STATUS, &status);
  if (status != GL_TRUE) {
    g_print ("Couldn't link GL program.\n");
    player.should_quit = TRUE;
    return;
  }
  glUseProgram (prog);
  player.win.gl_pos = 0;
  glBindAttribLocation (prog, player.win.gl_pos, "in_pos");
  player.win.gl_texcoord = player.win.gl_pos + 1;
  glBindAttribLocation (prog, player.win.gl_texcoord, "in_texcoord");
  player.win.gl_tex_uni = glGetUniformLocation (prog, "tex");
  glUniform1i (player.win.gl_tex_uni, 0);
  glLinkProgram (prog);

  if (!load_png_to_tex ("/background-4k.png", 0, OUTPUT_WIDTH, OUTPUT_HEIGHT)) {
    g_print ("Couldn't load background image into texture.\n");
    player.should_quit = TRUE;
    return;
  }

  /* load each surface's texture for display */
  for (i = 0; i < G_N_ELEMENTS (streams_data); i++) {
    MssStream *stream = &streams_data[i];
    if (!load_png_to_tex (stream->resource_name, i + 1, STREAM_WIDTH,
        STREAM_CAPTION)) {
      g_print ("Couldn't load %s for stream %d.\n", stream->resource_name,
          i);
      player.should_quit = TRUE;
      return;
    }
  }

  /* create a 1x1 buffer to attach to our subsurfaces, so they're mapped */
  pagesize = sysconf (_SC_PAGE_SIZE);
  fd = os_create_anonymous_file (pagesize);
  buffer_data = mmap (NULL, pagesize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (buffer_data == MAP_FAILED) {
    g_print ("Couldn't mmap blank buffer: %m\n");
    player.should_quit = TRUE;
    return;
  }
  memset (buffer_data, 0, pagesize);
  munmap (buffer_data, pagesize);

  wl_shm_pool = wl_shm_create_pool (player.dpy.wl_shm, fd, pagesize);
  player.dpy.blank_buffer = wl_shm_pool_create_buffer (wl_shm_pool, 0, 1, 1, 4,
      WL_SHM_FORMAT_ARGB8888);
  wl_shm_pool_destroy (wl_shm_pool);
}

static void
output_geometry (void *data, struct wl_output *output,
                 int32_t x, int32_t y,
                 int32_t physical_width, int32_t physical_height,
                 int32_t subpixel, const char *make, const char *model,
                 int32_t transform)
{
}

static void
output_mode (void *data, struct wl_output *output, uint32_t flags,
             int32_t width, int32_t height, int32_t refresh)
{
  if (!player.dpy.wl_output && (flags & WL_OUTPUT_MODE_CURRENT) &&
      width == OUTPUT_WIDTH && height == OUTPUT_HEIGHT) {
    player.dpy.wl_output = output;
  }
}

static void
output_done (void *data, struct wl_output *output)
{
  if (player.dpy.wl_output == output && player.dpy.output_scale == 0)
    player.dpy.output_scale = 1;
  player.dpy.roundtrips_required--;
}

static void
output_scale (void *data, struct wl_output *output, int32_t factor)
{
  /*
   * Weston sends its scale event before modes, so we can't rely on this
   * test being right. Typing out a full output abstraction didn't seem
   * like it was worth it, so if you're seeing weird scaling issues on
   * mixed-DPI systems, this is probably why ...
   * if (player.dpy.wl_output == output)
   */
    player.dpy.output_scale = factor;
}

static const struct wl_output_listener output_listener = {
  output_geometry,
  output_mode,
  output_done,
  output_scale
};

static void
shm_format (void *data, struct wl_shm *shm, uint32_t format)
{
  if (format == WL_SHM_FORMAT_ARGB8888)
    player.dpy.argb8888_supported = TRUE;
  player.dpy.roundtrips_required--;
}

static const struct wl_shm_listener shm_listener = {
  shm_format,
};

static MssTouch *
get_free_touchpoint (void)
{
  MssTouch *ret = NULL;

  for (int i = 0; i < G_N_ELEMENTS (player.dpy.touches); i++) {
    MssTouch *tmp = &player.dpy.touches[i];

    if (tmp->down_time == 0)
      return tmp; /* empty slot; take it */
    if (tmp->up_time == 0)
      continue; /* touch still in progress; leave it */

    if (!ret || tmp->up_time < ret->up_time)
      ret = tmp; /* our current best candidate */
  }

  return ret;
}

static MssTouch *
find_touchpoint (int32_t touch_id)
{
  for (int i = 0; i < G_N_ELEMENTS (player.dpy.touches); i++) {
    MssTouch *touch = &player.dpy.touches[i];
    if (touch->touch_id == touch_id && touch->up_time == 0)
      return touch;
  }

  return NULL;
}

static void
touch_down (void *data, struct wl_touch *wl_touch, uint32_t serial, uint32_t time,
    struct wl_surface *surface, int32_t id, wl_fixed_t sx, wl_fixed_t sy)
{
  MssTouch *touch = get_free_touchpoint ();
  int x = floor (wl_fixed_to_double (sx) * player.dpy.output_scale);
  int y = floor (wl_fixed_to_double (sy) * player.dpy.output_scale);

  if (!touch) {
    g_print ("Couldn't find a free touchpoint; use fewer hands?\n");
    return;
  }
  touch->touch_id = id;
  touch->down_time = time;
  touch->up_time = 0;
  touch->first_x = x;
  touch->first_y = y;
  touch->last_x = x;
  touch->last_y = y;

  for (int i = 0; i < G_N_ELEMENTS (streams_data); i++) {
    MssStream *stream = &streams_data[i];

    if (x < stream->cur_x || y < stream->cur_y ||
        x > stream->cur_x + STREAM_WIDTH ||
        y > stream->cur_y + STREAM_HEIGHT)
      continue;

    touch->stream = stream;
    return;
  }
}

static void
touch_up (void *data, struct wl_touch *wl_touch, uint32_t serial, uint32_t time,
    int32_t id)
{
  MssTouch *touch = find_touchpoint (id);
  int reset_taps = 0;

  if (!touch)
    return;

  touch->up_time = time;

  /* lame double-tap detection */
  for (int i = 0; i < G_N_ELEMENTS (player.dpy.touches); i++) {
    MssTouch *tap = &player.dpy.touches[i];
    if (tap->down_time == 0 || tap->up_time == 0)
      continue;
    if (tap->up_time - tap->down_time >= 250)
      continue;
    if (time - tap->up_time >= 500)
      continue;
    if (tap->last_x - tap->first_x >= 50 || tap->last_y - tap->first_y >= 50)
      continue;
    reset_taps++;    
  }  

  if (reset_taps >= 3) {
    for (int i = 0; i < G_N_ELEMENTS (player.dpy.touches); i++) {
      MssTouch *tap = &player.dpy.touches[i];
      tap->down_time = 0;
      tap->up_time = 0;
    }

    for (int i = 0; i < G_N_ELEMENTS (streams_data); i++) {
      MssStream *stream = &streams_data[i];
      stream->cur_x = stream->initial_x;
      stream->cur_y = stream->initial_y;
    }

    player.win.repaint_required = TRUE;
  }
}

static void
touch_motion (void *data, struct wl_touch *wl_touch, uint32_t time, int32_t id,
    wl_fixed_t sx, wl_fixed_t sy)
{
  MssTouch *touch = find_touchpoint (id);
  int x = floor (wl_fixed_to_double (sx) * player.dpy.output_scale);
  int y = floor (wl_fixed_to_double (sy) * player.dpy.output_scale);
  MssStream *stream;

  if (!touch)
    return;
  stream = touch->stream;

  if (stream) {
    stream->cur_x += (x - touch->last_x);
    if (stream->cur_x < 0)
      stream->cur_x = 0;
    if (stream->cur_x + STREAM_WIDTH > OUTPUT_WIDTH)
      stream->cur_x = OUTPUT_WIDTH - STREAM_WIDTH;
  
    stream->cur_y += (y - touch->last_y);
    if (stream->cur_y < 0)
      stream->cur_y = 0;
    if (stream->cur_y + STREAM_HEIGHT + STREAM_CAPTION > OUTPUT_HEIGHT)
      stream->cur_y = OUTPUT_HEIGHT - STREAM_HEIGHT - STREAM_CAPTION;
  
    player.win.repaint_required = TRUE;
  }

  touch->last_x = x;
  touch->last_y = y;
}

static void
touch_frame (void *data, struct wl_touch *touch)
{
}

static void
touch_cancel (void *data, struct wl_touch *touch)
{
  /* cancel means we have lost control over our touch stream and must
   * immediately release everything */
  for (int i = 0; i < G_N_ELEMENTS (player.dpy.touches); i++) {
    MssTouch *touch = &player.dpy.touches[i];
    touch->down_time = 0;
    touch->up_time = 0;
  }
}

static void
touch_shape (void *data, struct wl_touch *touch, int32_t id,
    wl_fixed_t major, wl_fixed_t minor)
{
}

static void
touch_orientation (void *data, struct wl_touch *touch, int32_t id,
    wl_fixed_t orientation)
{
}

static const struct wl_touch_listener wl_touch_listener = {
  touch_down,
  touch_up,
  touch_motion,
  touch_frame,
  touch_cancel,
  touch_shape,
  touch_orientation,
};

static void
seat_capabilities (void *data, struct wl_seat *seat, uint32_t capabilities)
{
  gboolean old_touch = !!player.dpy.wl_touch;
  gboolean new_touch = !!(capabilities & WL_SEAT_CAPABILITY_TOUCH);

  if (!old_touch && new_touch) {
    player.dpy.wl_touch = wl_seat_get_touch (seat);
    wl_touch_add_listener (player.dpy.wl_touch, &wl_touch_listener, NULL);
  }
  if (old_touch && !new_touch) {
    wl_touch_destroy (player.dpy.wl_touch);
    player.dpy.wl_touch = NULL;
  }

  player.dpy.roundtrips_required--;
}

static void
seat_name (void *data, struct wl_seat *seat, const char *name)
{
}

static const struct wl_seat_listener wl_seat_listener = {
  seat_capabilities,
  seat_name,
};

static void
registry_global (void *data, struct wl_registry *registry, uint32_t name,
                 const char *interface, uint32_t version)
{
  if (strcmp (interface, "wl_compositor") == 0) {
    player.dpy.wl_compositor = wl_registry_bind (registry, name,
        &wl_compositor_interface, MIN (version, 4));
  } else if (strcmp (interface, "wl_output") == 0) {
    /* we don't add this to the struct yet because we want to iterate the
     * outputs to find a 4K one */
    struct wl_output *output = wl_registry_bind (registry, name,
        &wl_output_interface, MIN (version, 3));
    wl_output_add_listener (output, &output_listener, NULL);
    player.dpy.roundtrips_required++;
  } else if (strcmp (interface, "wl_shm") == 0) {
    player.dpy.wl_shm = wl_registry_bind (registry, name, &wl_shm_interface,
        MIN (version, 1));
    wl_shm_add_listener (player.dpy.wl_shm, &shm_listener, NULL);
    player.dpy.roundtrips_required++;
  } else if (strcmp (interface, "wl_subcompositor") == 0) {
    player.dpy.wl_subcompositor = wl_registry_bind (registry, name,
        &wl_subcompositor_interface, MIN (version, 1));
  } else if (strcmp (interface, "xdg_wm_base") == 0) {
    player.dpy.xdg_wm_base = wl_registry_bind (registry, name,
        &xdg_wm_base_interface, MIN (version, 2));
    xdg_wm_base_add_listener (player.dpy.xdg_wm_base, &xdg_wm_base_listener,
        NULL);
  } else if (strcmp (interface, "wl_seat") == 0) {
    player.dpy.wl_seat = wl_registry_bind (registry, name,
        &wl_seat_interface, MIN (version, 7));
    wl_seat_add_listener (player.dpy.wl_seat, &wl_seat_listener, NULL);
    player.dpy.roundtrips_required++;
  }
}

static void
registry_remove (void *data, struct wl_registry *registry, uint32_t name)
{
}

static const struct wl_registry_listener registry_listener = {
  registry_global,
  registry_remove
};

static gboolean
egl_init (void)
{
  PFNEGLGETPLATFORMDISPLAYEXTPROC get_platform_display;
  const char *client_exts = eglQueryString (EGL_NO_DISPLAY, EGL_EXTENSIONS);
  EGLint config_attrs[] = {
    EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
    EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
    EGL_ALPHA_SIZE, 8,
    EGL_RED_SIZE, 8,
    EGL_GREEN_SIZE, 8,
    EGL_BLUE_SIZE, 8,
    EGL_BUFFER_SIZE, 32,
    EGL_NONE
  };
  EGLint context_attrs[] = {
    EGL_CONTEXT_CLIENT_VERSION, 2,
    EGL_NONE
  };
  EGLint n;
  EGLBoolean ret;

  if (!client_exts) {
    g_print ("EGL_EXT_client_extensions not supported\n");
    return FALSE;
  }

  /* We could use the EGL 1.5 core variants here instead, but then we'd have to
   * type up support for two entrypoints, so here we are. */
  if (!check_egl_extension (client_exts, "EGL_EXT_platform_base")) {
    g_print ("EGL_EXT_platform_base not supported\n");
    return FALSE;
  }

  if (!check_egl_extension (client_exts, "EGL_EXT_platform_wayland") &&
      !check_egl_extension (client_exts, "EGL_KHR_platform_wayland")) {
    g_print ("EGL_KHR_platform_wayland not supported\n");
    return FALSE;
  }

  get_platform_display =
      (PFNEGLGETPLATFORMDISPLAYEXTPROC) eglGetProcAddress ("eglGetPlatformDisplayEXT");
  player.dpy.egl_display = get_platform_display (EGL_PLATFORM_WAYLAND_KHR,
      player.dpy.wl_display, NULL);
  if (!player.dpy.egl_display ||
      !eglInitialize(player.dpy.egl_display, NULL, NULL)) {
    g_print ("Failed to create EGL display.\n");
    return FALSE;
  }

  ret = eglChooseConfig (player.dpy.egl_display, config_attrs,
      &player.win.egl_config, 1, &n);
  if (ret == FALSE || n == 0) {
    g_print ("Failed to find EGL config.\n");
    return FALSE;
  }

  eglBindAPI(EGL_OPENGL_ES_API);
  player.win.egl_context = eglCreateContext (player.dpy.egl_display,
      player.win.egl_config, EGL_NO_CONTEXT, context_attrs);
  if (player.win.egl_context == EGL_NO_CONTEXT) {
    g_print ("Failed to create EGL context.\n");
    return FALSE;
  }

  return TRUE;
}

gint
main (gint argc, gchar **argv)
{
  GError *error = NULL;
  GOptionContext *context;
  GOptionEntry entries[] = {
    { "videotestsrc", 't', 0, G_OPTION_ARG_NONE, &player.videotestsrc, "Use videotestsrc", NULL },
    { "mss-server", 's', 0, G_OPTION_ARG_STRING, &player.mss_server, "Specify the multistream server address",  NULL },
    { NULL }
  };

  context = g_option_context_new ("- multistream player");
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_add_group (context, gst_init_get_option_group ());
  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    g_print ("option parsing failed: %s\n", error->message);
    exit (1);
  }

  player.resources = multistream_player_get_resource ();
  g_resources_register (player.resources);

  player.dpy.wl_display = wl_display_connect (NULL);
  if (!player.dpy.wl_display)
    return 1;
  player.dpy.wl_registry = wl_display_get_registry (player.dpy.wl_display);
  wl_registry_add_listener (player.dpy.wl_registry, &registry_listener, &player);
  player.dpy.roundtrips_required = 1; /* get available extensions */
  while (!player.should_quit && player.dpy.roundtrips_required-- > 0) {
    int ret = wl_display_roundtrip (player.dpy.wl_display);
    if (ret < 0)
      player.should_quit = TRUE;
  }
  if (player.should_quit)
    return 1;

  if (!validate_system ()) {
    g_print ("System does not meet requirements. Exiting.\n");
    return 1;
  }

  if (!egl_init ()) {
    g_print ("Failed to intiialise EGL. Exiting.\n");
    return 1;
  }

  setup_surfaces ();
  setup_streams ();

  player.win.repaint_required = TRUE;
  while (!player.should_quit) {
    int ret = wl_display_dispatch (player.dpy.wl_display);
    if (ret < 0)
      break;

    if (player.win.repaint_required && !player.win.frame)
      repaint ();
  }

  stop_streams ();

  return 0;
}
